# Simple CSV Splitter

A Python script to quickly split a CSV file into many smaller files

Features:
 - Simple: Under 40 lines of well-commented Python allows this script to be easily audited for use in an environment handling sensitive data.
 - Support for various header sizes: Copy a multi-line header to the top of each split file automatically
 - Able to handle large data sets: Splitting a 5,429,253 line long CSV file into chunks of 100 took 3 minutes and 27 seconds, producing a total of 54,293 files and never exceeding 3.3 megabytes of RAM used.

## Requirements

This script requires Python 3. No python modules are required.

## Usage

Start by putting your CSV file in the same directory as this program. Run with `python main.py` (most Windows/MacOS installations) or `python3 main.py` (most Linux distributions). When prompted, input how many rows should be treated as headers, how many rows (not including headers) you want in each file, the filename of the input CSV, and the filename of the output CSV.