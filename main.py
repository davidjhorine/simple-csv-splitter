def main():
    headerLength = int(input('How many rows is your header? '))
    outputLength = int(input('What would you like the length of your output (not including header) to be? '))
    fileName = input('What is the name of your CSV file? ')
    outputName = input('What is the name of your output files? (using exampleFileName would result in exampleFileName1.csv, exampleFileName2.csv, etc) ')
    counter = 1 # This is used to determine when the file should be split.
    splitNum = 0 # This is used to determine the naming of each file.
    headers = [] # This is used to store the header lines of the file.
    lines = [] # This is used to store the currently held lines.
    with open(fileName, 'r') as csvFile:
        for line in csvFile:
            # This section will determine whether to send the line to the headers array or the lines array.
            if counter <= headerLength:
                headers.append(line)
            else:
                lines.append(line)
            # This section determines when to split the file.
            # Making sure the counter is at least one prevents
            # the file from getting split when the counter is equal to
            # the header length, which would leave a file of just headers.
            if (((counter - headerLength) % outputLength) == 0) and ((counter - headerLength) > 0):
                splitNum += 1
                with open (outputName + str(splitNum) + '.csv' ,'a') as outputFile:
                    for row in headers: outputFile.write(row)
                    for row in lines: outputFile.write(row)
                lines = []
            counter += 1
        # This section will create a new file if the lines array isn't empty when the splitting is done.
        if lines != []:
            splitNum += 1
            with open (outputName + str(splitNum) + '.csv' ,'a') as outputFile:
                for row in headers: outputFile.write(row)
                for row in lines: outputFile.write(row)
            lines = []
        # This provides some statistics just for confirmation that it worked.
        print('Split ' + str(counter) + ' lines into ' + str(splitNum) + ' files.')

if __name__ == '__main__':
    main()